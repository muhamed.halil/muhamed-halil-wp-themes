<?php
function simple_theme_setup(){
	// Featrued Image http_support()
	add_theme_support( 'post-thumbnails' );

	// Menus
	register_nav_menus( array(
		'primary' => __('Primary Menu')
	) );
}

add_action( 'after_setup_theme', 'simple_theme_setup');

// Excerpt length
function set_excerpt_length(){
	return 75;
}

add_filter( 'excerpt_length', 'set_excerpt_length');

//Widget Locations
function init_widgets($id){

	/**
	 * Creates a sidebar
	 * @param string|array  Builds Sidebar based off of 'name' and 'id' values.
	 */
	$args = array(
		'name'          => __( 'Sidebar', 'text-domain' ),
		'id'            => 'sidebar',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="side-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	);

	register_sidebar( $args );

}

add_action('widgets_init', 'init_widgets');
